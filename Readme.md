### Authentication with JWT
- JSON Web Tokens are ONE way to implement authentication
-  npm init -y
-  npm install express mongoose ejs validator

#### Auth Routes & Controllers
- Using an MVC (model,view & control) approach


### Mongoose Hooks
- open `user.js` in models folder
```sh
// fire a function after doc saved to db
userSchema.post('save', function (doc, next) {
  console.log('new user was created & saved', doc);
  next();
});

// fire a function before doc saved to db
userSchema.pre('save', async function(next) {
  const salt = await bcrypt.genSalt();
  this.password = await bcrypt.hash(this.password, salt);
  next();
});
```

### Hashing Passwords
- npm install bcrypt
- open `user.js` in models folder
```sh
// fire a function before doc saved to db
userSchema.pre('save', async function(next) {
  const salt = await bcrypt.genSalt();
  this.password = await bcrypt.hash(this.password, salt);
  next();
});
```

### Cookies Primer
- Store Data in a user's browser
  - name=idrees, age=20, isEmployee=true
```sh
// cookies
const cookieParser = require('cookie-parser');
app.use(cookieParser());

app.get('/set-cookies', (req, res) => {

  // res.setHeader('Set-Cookie', 'newUser=true');
  
  res.cookie('newUser', false);
  res.cookie('isEmployee', true, { maxAge: 1000 * 60 * 60 * 24, httpOnly: true });
  res.cookie('isEmployee', true, { maxAge: 1000 * 60 * 60 * 24, secure: true });

  res.send('you got the cookies!');

});

app.get('/read-cookies', (req, res) => {

  const cookies = req.cookies;
  console.log(cookies.newUser);

  res.json(cookies);

});
```
### JSON Web Tokens (theory)
- #### Headers
  - Tells the server what type of signature is being used (meta)
- #### Payload
  - Used to identify the user (e.g. contains user id)
- #### Signature
  - Makes the token secure (like a stamp of authenticity)
  
### New User Signup
- open `app.js`
```sh
const cookieParser = require('cookie-parser');
app.use(cookieParser());
```
- open `signup.ejs`
```sh
<script>
  const form = document.querySelector('form');
  form.addEventListener('submit', (e) => {
    e.preventDefault();
    // get values
    const email = form.email.value;
    const password = form.password.value;
    try {
      const res = await fetch('/signup', {
        method: 'POST',
        body: JSON.stringify({
          email,
          password
        }),
        headers: {
          'Content-Type': 'application/json'
        }
      });
    } catch (err) {
      console.log(err);
    }
  });
</script>
```
- npm install jsonwebtoken
- open `authController.js`
```sh
// create json web token
const maxAge = 3 * 24 * 60 * 60;
const createToken = (id) => {
  return jwt.sign({ id }, '3z0xHPVxCjyUx3qhf5SFUIGCNAEzfil8', {
    expiresIn: maxAge
  });
};

  }

```

### Logging Users

